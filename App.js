import React from 'react';
import { View, Text, Button } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import * as Font from 'expo-font';
import Navigator from './navigation/Navigator.js';


export default class App extends React.Component {
  state = {
    fontLoaded: false,
  };

  async componentDidMount() {
    await Font.loadAsync({
      'Montserrat-Regular': require('./assets/fonts/Montserrat-Regular.ttf'),
      'Montserrat-Bold': require('./assets/fonts/Montserrat-Bold.ttf'),
    });
    this.setState({ fontLoaded: true });
  } 
  render() {
    {
      return (
         this.state.fontLoaded ? (
          <Navigator />
      ) : <></>
      )
    }
  }
}