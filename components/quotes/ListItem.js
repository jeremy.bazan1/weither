import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image
} from "react-native";
import { Icon } from "react-native-elements";

export default class QuotesListItem extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {};

    return {
      header: null
    };
  };

  render() {
    return (
          <TouchableOpacity
            // onPress={() => this.props.navigation.navigate("TrumpQuoteDetail")}
          >
            <View
              style={{
                flex: 1,
                marginTop: 10,
                marginBottom: 10,
                alignItems: "center",
                backgroundColor: "#FFFFFF",
                opacity: 0.9,
                width: 336,
                borderRadius: 12
              }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "space-evenly",
                  alignItems: "center"
                }}
              >
                <View style={{ width: "25%", alignContent: "center" }}>
                  <Image
                    style={{ position: "relative", margin: 10 }}
                    source={require("../../assets/image/trump-el-paso-baby.png")}
                  />
                </View>
                <View style={{ width: "40%" }}>
                  <Text
                    style={{
                      color: "#404491",
                      fontStyle: "italic",
                      textAlign: "left",
                      fontFamily: "Montserrat-Regular",
                      fontSize: 16
                    }}
                  >
                    {this.props.quote.date}
                  </Text>
                  <Text
                    style={{
                      color: "#404491",
                      fontStyle: "italic",
                      textAlign: "left",
                      fontFamily: "Montserrat-Regular",
                      fontSize: 16
                    }}
                  >
                    {this.props.quote.hour}
                  </Text>
                </View>
                <View
                  style={{ width: "15%", alignContent: "flex-start" }}
                ></View>
                <View style={{ width: "20%" }}>
                  <Icon
                    name="chevron-right"
                    size={40}
                    color="#333675"
                    onPress={() =>
                      this.props.navigation.navigate("TrumpQuoteDetail", {
                        data: "un detail"
                      })
                    }
                  />
                </View>
              </View>
              <View>
                <Text
                  style={{
                    color: "#404491",
                    fontWeight: "bold",
                    fontStyle: "italic",
                    textAlign: "left",
                    fontFamily: "Montserrat-Regular",
                    fontSize: 18,
                    padding: 15
                  }}
                >
                  {this.props.quote.text}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
    );
  }
}
