import React from 'react';
import QuotesListItem from './ListItem';
import {
  ScrollView,
} from "react-native";

export default class QuoteList extends React.Component {

  constructor(props) {
    super(props);
    
    this.data = [
      {
        date: "14 Septembre 2019",
        hour: "06h01",
        text: "blablablabla"
      },
      {
        date: "10 Septembre 2019",
        hour: "14h30",
        text: "bleblebleble"
      },
      {
        date: "31 Septembre 2019",
        hour: "19h12",
        text: "blublublublu"
      }
      
    ];    
  };

  renderQuotesItem = () => {
    // console.log(this.props)
    return this.data.map((item, index) => {
      return <QuotesListItem
        key={index}
        quote={item}
      />
    })
  };


  render() {
    return (
      <ScrollView>
        {this.renderQuotesItem()}
      </ScrollView>
    )
  }

}
